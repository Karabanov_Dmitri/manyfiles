import java.io.*;

/**
 * class manyFiles3 создаёт файл:txt6data.dat
 * class manyFiles3 открывает файл:int6data.dat
 * запускает метод проверки на "счастливого билета"  и отправляет правельные числа в файл txt6data.dat
 *
 * @author Karabanov group 15oit18
 */

public class manyFiles3 {
    public static void main(String[] args) throws IOException {
        File txt6data = new File("src\\txt6data.dat");
        DataInputStream dataReader = new DataInputStream(new FileInputStream("src\\int6data.dat"));
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(txt6data));
        int number;
        while (dataReader.available() > 0) {
            number = dataReader.readInt();
            if (isLuckyNumber(number)) {
                bufferedWriter.write(number + "\n");
            }
        }

        dataReader.close();
        bufferedWriter.close();
    }

    private static boolean isLuckyNumber(int number) {
        int[] array = new int[6];
        int denum = 1;
        int i = 0;
        for (i = 0; i < 6; i++) {
            if (i == 5) {
                array[i] = number / denum;
            } else {
                array[i] = number / denum % 10;
            }
            denum = denum * 10;
        }
        return array[0] + array[1] + array[2] == array[3] + array[4] + array[5];
    }

}