import java.io.*;

/**
 * class manyFiles2 создаёт файл:int6data.dat
 * класс проверяет возможность подсчета "счастливого билета"
 * записивыет в новый файл int6data.dat данные из класса
 *
 * @author Karabanov group 15oit18
 */

public class manyFiles2 {
    public static void main(String[] args) throws IOException {
        File int6data = new File("src\\int6data.dat");
        DataOutputStream dataOutputStream = new DataOutputStream(new FileOutputStream(int6data));
        DataInputStream dataReader = new DataInputStream(new FileInputStream("src\\intdata.dat"));
        int number;
        while (dataReader.available() > 0) {
            number = dataReader.readInt();
            if (number == 0) {
                dataOutputStream.writeInt(number);
            } else if (number > 999 && number < 1000000) {
                dataOutputStream.writeInt(number);
            }
        }

        dataReader.close();
        dataOutputStream.close();
    }
}