import java.io.*;

/**
 * class manyFiles создаёт файлы: intdata.dat; error.dat.
 * читает из файла input.txt входные значения
 * проверяет исключение (неверность входных данных)
 *
 * @author Karabanov group 15oit18
 */

public class manyFiles {
    public static void main(String[] args) throws IOException {
        File intdata = new File("src\\intdata.dat");
        File error = new File("src\\error.dat");
        BufferedReader bufferedReader = new BufferedReader(new FileReader("src\\input.txt"));
        DataOutputStream dataWriter = new DataOutputStream(new FileOutputStream(intdata));
        BufferedWriter errorWriter = new BufferedWriter(new FileWriter(error));
        String string;
        int number;
        while ((string = bufferedReader.readLine()) != null) {
            try {
                number = Integer.parseInt(string);
                dataWriter.writeInt(number);
            } catch (NumberFormatException e) {
                errorWriter.write(e + "\n");
            }

        }

        bufferedReader.close();
        dataWriter.close();
        errorWriter.close();
    }
}